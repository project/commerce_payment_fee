<?php

namespace Drupal\commerce_payment_fee;

use Drupal\Core\Entity\EntityInterface;

class PaymentListBuilder extends \Drupal\commerce_payment\PaymentListBuilder {

  public function buildHeader() {
    $header = parent::buildHeader();
    $new_header = [];
    $new_header['label'] = array_shift($header);
    $new_header['commerce_payment_fee_amount'] = $this->t('Processing Fee');
    $new_header['commerce_payment_fee_net_amount'] = $this->t('Net Amount');
    $new_header += $header;
    return $new_header;
  }

  public function buildRow(EntityInterface $entity) {
    $row = parent::buildRow($entity);
    $new_row = [];
    $new_row['label'] = array_shift($row);
    if (!$entity->get('commerce_payment_fee_amount')->isEmpty()) {
      $new_row['commerce_payment_fee_amount'] = $this->currencyFormatter->format($entity->get('commerce_payment_fee_amount')->number, $entity->get('commerce_payment_fee_amount')->currency_code);
    }
    else {
      $new_row['commerce_payment_fee_amount'] = '';
    }
    if (!$entity->get('commerce_payment_fee_net_amount')->isEmpty()) {
      $new_row['commerce_payment_fee_net_amount'] = $this->currencyFormatter->format($entity->get('commerce_payment_fee_net_amount')->number, $entity->get('commerce_payment_fee_net_amount')->currency_code);
    }
    else {
      $new_row['commerce_payment_fee_net_amount'] = '';
    }
    $new_row += $row;
    return $new_row;
  }

}
